package com.zweirm.lagee_quizing.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Spring Security配置类
 */
@ConfigurationProperties(prefix = "lagee.security")
public class SecurityProperties {
    // 注册BrowserProperties
    private BrowserProperties browserProperties = new BrowserProperties();

    public BrowserProperties getBrowserProperties() {
        return browserProperties;
    }

    public void setBrowserProperties(BrowserProperties browserProperties) {
        this.browserProperties = browserProperties;
    }
}
