package com.zweirm.lagee_quizing.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class StudentUser {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    @ManyToOne
    @JoinTable(name = "clazz_student_user", joinColumns = @JoinColumn(name = "student_user_id"), inverseJoinColumns = @JoinColumn(name = "clazz_id"))
    private Clazz clazz;
    @OneToMany(mappedBy = "studentUser")
    private List<Result> results;
    private String email;
    private String password;
    private String phone;

    public StudentUser() {
    }

    public StudentUser(String name, Clazz clazz, List<Result> results, String email, String password, String phone) {
        this.name = name;
        this.clazz = clazz;
        this.results = results;
        this.email = email;
        this.password = password;
        this.phone = phone;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Clazz getClazz() {
        return clazz;
    }

    public void setClazz(Clazz clazz) {
        this.clazz = clazz;
    }

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
