package com.zweirm.lagee_quizing.domain;

import javax.persistence.*;

@Entity
public class Choice {
    @Id
    @GeneratedValue
    private Long id;
    private String content;
    private Boolean isRight;
    private String illustration;
    @ManyToOne
    @JoinTable(name = "question_choice", joinColumns = @JoinColumn(name = "choice_id"), inverseJoinColumns = @JoinColumn(name = "question_id"))
    private Question question;
    private String status;

    public Choice() {
    }

    public Choice(String content, Boolean isRight, String illustration, Question question, String status) {
        this.content = content;
        this.isRight = isRight;
        this.illustration = illustration;
        this.question = question;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean getRight() {
        return isRight;
    }

    public void setRight(Boolean right) {
        isRight = right;
    }

    public String getIllustration() {
        return illustration;
    }

    public void setIllustration(String illustration) {
        this.illustration = illustration;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
