package com.zweirm.lagee_quizing.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class Question {
    @Id
    @GeneratedValue
    private Long id;
    private String title;
    @ManyToOne
    @JoinTable(name = "paper_question", joinColumns = @JoinColumn(name = "question_id"), inverseJoinColumns = @JoinColumn(name = "paper_id"))
    private Paper paper;
    @OneToMany(mappedBy = "question")
    private List<Choice> choices;
    private String status;

    public Question() {
    }

    public Question(String title, Paper paper, List<Choice> choices, String status) {
        this.title = title;
        this.paper = paper;
        this.choices = choices;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Paper getPaper() {
        return paper;
    }

    public void setPaper(Paper paper) {
        this.paper = paper;
    }

    public List<Choice> getChoices() {
        return choices;
    }

    public void setChoices(List<Choice> choices) {
        this.choices = choices;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
