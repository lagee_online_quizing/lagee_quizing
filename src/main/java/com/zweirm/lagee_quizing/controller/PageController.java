package com.zweirm.lagee_quizing.controller;

import com.zweirm.lagee_quizing.domain.Clazz;
import com.zweirm.lagee_quizing.domain.Paper;
import com.zweirm.lagee_quizing.domain.Quiz;
import com.zweirm.lagee_quizing.service.PaperService;
import com.zweirm.lagee_quizing.service.QuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class PageController {
    @Autowired
    private PaperService paperService;

    @Autowired
    private QuizService quizService;

    @RequestMapping("/backLogin")
    public String goBackLogin() {
        return "/backLogin";
    }

    @RequestMapping("/main")
    public String goMain() {
        return "/main";
    }

    @RequestMapping("/register")
    public String goRegister() {
        return "/register";
    }

    @RequestMapping("/login")
    public String goLogin() {
        return "/login";
    }

    @RequestMapping("/goPaper")
    public String goPaper() {
        return "/main_paper";
    }

    @RequestMapping("/goQuiz")
    public String goQuiz() {
        return "/main_quiz";
    }

    @RequestMapping("/result")
    public String goForResult(Model model) {
        List<Quiz> quizzes = quizService.findAllQuiz("1");
        model.addAttribute("quizzes", quizzes);
        return "/main_result";
    }

    @RequestMapping("/newPaper")
    public String goNewPaper() {
        return "/paper_newPaper";
    }

    @RequestMapping("/paperUpdate/{id}")
    public String goUpdate(@PathVariable Long id, Model model) {
        Paper paper = paperService.findPaperById(id);
        model.addAttribute("id", paper.getId());
        model.addAttribute("name", paper.getName());
        model.addAttribute("description", paper.getDescription());
        model.addAttribute("type", paper.getType());
        model.addAttribute("questions", paper.getQuestions());
        model.addAttribute("name", paper.getName());
        model.addAttribute("amount", paper.getAmount());
        model.addAttribute("status", paper.getStatus());
        return "/paper_update";
    }

    @RequestMapping("/paper_details")
    public String goPaperDetails() {
        return "/paper_details";
    }

    @RequestMapping("/paper_question")
    public String goPaperQuestion() {
        return "/paper_question";
    }

    @RequestMapping("/newQuestion/{paperId}")
    public String saveQuestion(@PathVariable Long paperId, Model model) {
        Paper paper = paperService.findPaperById(paperId);
        model.addAttribute("id", paper.getId());
        model.addAttribute("name", paper.getName());
        model.addAttribute("description", paper.getDescription());
        model.addAttribute("type", paper.getType());
        return "/question_newQuestion";
    }

    @RequestMapping("/newQuiz")
    public String goNewQuiz(Model model) {
        List<Paper> papers = paperService.showAll("1");
        model.addAttribute("papers", papers);
        return "/quiz_newQuiz";
    }

    @RequestMapping("/quizUpdate/{quizId}")
    public String goQuizUpdate(@PathVariable Long quizId, Model model) {
        Quiz quiz = quizService.quizDetails(quizId);
        List<Paper> papers = paperService.showAll("1");

        List<Clazz> clazzes = quiz.getClazzes();
        List<String> clazzNames = new ArrayList<>();
        for (Clazz clazz : clazzes) {
            clazzNames.add(clazz.getName());
        }
        Paper paper = quiz.getPaper();

        model.addAttribute("title", quiz.getTitle());
        model.addAttribute("type", quiz.getType());
        model.addAttribute("clazz", clazzNames);
        model.addAttribute("destPaper", paper.getName());
        model.addAttribute("papers", papers);
        model.addAttribute("quizId", quizId);
        return "/quiz_update";
    }
}
