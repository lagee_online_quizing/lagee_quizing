package com.zweirm.lagee_quizing.controller;

import com.zweirm.lagee_quizing.domain.Choice;
import com.zweirm.lagee_quizing.domain.Paper;
import com.zweirm.lagee_quizing.domain.Question;
import com.zweirm.lagee_quizing.service.PaperService;
import com.zweirm.lagee_quizing.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class QuestionController {
    @Autowired
    private QuestionService questionService;

    @Autowired
    private PaperService paperService;

    @RequestMapping("/question/{id}")
    public String getQuestions(@PathVariable Long id, Model model) {
        List<Question> questions = questionService.findAllByPaper_IdAndStatus(id, "1");
        Paper paper = paperService.findPaperById(id);
        model.addAttribute("name", paper.getName());
        model.addAttribute("description", paper.getDescription());
        model.addAttribute("type", paper.getType());
        model.addAttribute("questions", questions);
        model.addAttribute("paperId", id);
        return "forward:/paper_question";
    }

    @RequestMapping("/saveQuestion")
    public String saveQuestion(
            String title,
            String isRight,
            String answer_1_input,
            String answer_2_input,
            String answer_3_input,
            String answer_4_input,
            String answer_1_illus,
            String answer_2_illus,
            String answer_3_illus,
            String answer_4_illus,
            Long paperId,
            Model model) {
        questionService.saveQuestion(
                title,
                isRight,
                answer_1_input,
                answer_2_input,
                answer_3_input,
                answer_4_input,
                answer_1_illus,
                answer_2_illus,
                answer_3_illus,
                answer_4_illus,
                paperId);
        Paper paper = paperService.findPaperById(paperId);
        model.addAttribute("name", paper.getName());
        model.addAttribute("description", paper.getDescription());
        model.addAttribute("type", paper.getType());
        return "forward:/question/" + paperId;
    }

    @RequestMapping("/questionDetails/{questionId}")
    public String showQuestionDetails(@PathVariable Long questionId, Model model) {
        model.addAttribute(questionId);
        Question question = questionService.findQuestion(questionId);
        String title = question.getTitle();
        model.addAttribute("title", title);

        List<Choice> choices = questionService.findChoices(questionId);
        for (int i = 0; i < choices.size(); i++) {
            Choice choice = choices.get(i);
            String content = choice.getContent();
            String illustration = choice.getIllustration();
            Boolean isRight = choice.getRight();

            System.out.println(content);
            System.out.println(isRight);

            model.addAttribute("choice_" + (i + 1) + "_isRight", isRight.toString());
            model.addAttribute("choice_" + (i + 1) + "_content", content);
            model.addAttribute("choice_" + (i + 1) + "_illustration", illustration);
        }
        return "/question_details";
    }

    @RequestMapping("/questionUpdate/{questionId}")
    public String goUpdateQuestion(@PathVariable Long questionId, Model model) {
        showQuestionDetails(questionId, model);
        return "/question_update";
    }

    @RequestMapping("/updateQuestion")
    public String updateQuestion(String title,
                                 String isRight,
                                 String answer_1_input,
                                 String answer_2_input,
                                 String answer_3_input,
                                 String answer_4_input,
                                 String answer_1_illus,
                                 String answer_2_illus,
                                 String answer_3_illus,
                                 String answer_4_illus,
                                 Long questionId) {
        questionService.updateQuestion(
                title,
                isRight,
                answer_1_input,
                answer_2_input,
                answer_3_input,
                answer_4_input,
                answer_1_illus,
                answer_2_illus,
                answer_3_illus,
                answer_4_illus,
                questionId);
        return "forward:/questionDetails/" +questionId;
    }

    @RequestMapping("/questionDelete/{paperId}/{questionId}")
    public String deleteQuestion(@PathVariable Long paperId, @PathVariable Long questionId, Model model) {
        questionService.deleteQuestion(questionId);
        showQuestionDetails(questionId, model);
        return "redirect:/question/" + paperId;
    }
}
