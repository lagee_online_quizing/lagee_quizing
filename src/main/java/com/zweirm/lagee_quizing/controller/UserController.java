package com.zweirm.lagee_quizing.controller;

import com.zweirm.lagee_quizing.domain.User;
import com.zweirm.lagee_quizing.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("/doRegister")
    public String register(User user) {
        userService.saveUser(user);
        return "/login";
    }

    @RequestMapping("/checkUserExist")
    @ResponseBody
    public User checkUserExist(String email) {
        return userService.findUserByEmail(email);
    }
}
