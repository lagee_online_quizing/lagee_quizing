package com.zweirm.lagee_quizing.controller;

import com.zweirm.lagee_quizing.domain.Paper;
import com.zweirm.lagee_quizing.service.PaperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class PaperController {
    @Autowired
    private PaperService paperService;

    @RequestMapping("/paper")
    public String showAll(Model model) {
        List<Paper> papers = paperService.showAll("1");
        model.addAttribute("papers", papers);
        return "forward:/goPaper";
    }

    @RequestMapping("/savePaper")
    public String savePaper(Paper paper) {
        paperService.savePaper(paper);
        return "redirect:/paper";
    }

    @RequestMapping("/paperDetails/{id}")
    public String showDetails(@PathVariable Long id, Model model) {
        Paper paper = paperService.findPaperById(id);
        model.addAttribute("id", paper.getId());
        model.addAttribute("name", paper.getName());
        model.addAttribute("description", paper.getDescription());
        model.addAttribute("type", paper.getType());
        model.addAttribute("questions", paper.getQuestions());
        model.addAttribute("amount", paper.getAmount());
        model.addAttribute("status", paper.getStatus());
        return "forward:/paper_details";
    }

    @RequestMapping("/updatePaper")
    public String updatePaper(String name, String description, String type, Long id, Model model) {
        paperService.updatePaper(name, description, type, id);
        showDetails(id, model);
        return "forward:/paper_details";
    }

    @RequestMapping("/deletePaper/{id}")
    public String deletePaper(@PathVariable Long id, Model model) {
        paperService.deletePaper(id);
        showDetails(id, model);
        return "redirect:/paper";
    }
}
