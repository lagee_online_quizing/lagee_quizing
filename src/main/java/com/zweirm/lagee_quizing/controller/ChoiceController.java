package com.zweirm.lagee_quizing.controller;

import com.zweirm.lagee_quizing.domain.Choice;
import com.zweirm.lagee_quizing.service.ChoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@CrossOrigin
public class ChoiceController {
    @Autowired
    private ChoiceService choiceService;

    @RequestMapping("/findChoices")
    @ResponseBody
    public void getChoices(Long id, HttpServletRequest request, Model model) {
        HttpSession session = request.getSession();
        session.removeAttribute("choices");
        List<Choice> choices = choiceService.findByQuestion_Id(id);
        session.setAttribute("choices", choices);
    }
}
