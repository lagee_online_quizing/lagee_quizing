package com.zweirm.lagee_quizing.repository;

import com.zweirm.lagee_quizing.domain.Paper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaperRepository extends JpaRepository<Paper, Long> {
    List<Paper> findAllByStatus(String status);

    Paper findPaperById(Long id);

    Paper findPaperByName(String name);
}
