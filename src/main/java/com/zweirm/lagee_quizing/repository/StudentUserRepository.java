package com.zweirm.lagee_quizing.repository;

import com.zweirm.lagee_quizing.domain.Clazz;
import com.zweirm.lagee_quizing.domain.StudentUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentUserRepository extends JpaRepository<StudentUser, Long> {
    List<StudentUser> findByClazz(Clazz clazz);
}
