package com.zweirm.lagee_quizing.repository;

import com.zweirm.lagee_quizing.domain.Quiz;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuizRepository extends JpaRepository<Quiz, Long> {
    List<Quiz> findAllByStatus(String status);

    Quiz findQuizById(Long quizId);
}
