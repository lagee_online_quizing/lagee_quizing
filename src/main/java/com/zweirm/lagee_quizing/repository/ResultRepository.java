package com.zweirm.lagee_quizing.repository;

import com.zweirm.lagee_quizing.domain.Quiz;
import com.zweirm.lagee_quizing.domain.Result;
import com.zweirm.lagee_quizing.domain.StudentUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResultRepository extends JpaRepository<Result, Long> {
    Result findByQuizAndStudentUser(Quiz quiz, StudentUser studentUser);
}
