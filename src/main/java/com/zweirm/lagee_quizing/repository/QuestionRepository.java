package com.zweirm.lagee_quizing.repository;

import com.zweirm.lagee_quizing.domain.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {
    Long findQuestionIdByPaper_Id(Long id);

    List<Question> findQuestionByPaper_IdAndChoices_Id(Long paper_id, Long choices_id);

    List<Question> findAllByPaper_IdAndStatus(Long id, String status);

    Question findQuestionById(Long id);
}
