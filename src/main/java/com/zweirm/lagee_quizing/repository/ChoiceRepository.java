package com.zweirm.lagee_quizing.repository;

import com.zweirm.lagee_quizing.domain.Choice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChoiceRepository extends JpaRepository<Choice, Long> {
    List<Choice> findChoiceByQuestion_Id(Long id);
}
