package com.zweirm.lagee_quizing.repository;

import com.zweirm.lagee_quizing.domain.Clazz;
import com.zweirm.lagee_quizing.domain.Quiz;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClazzRepository extends JpaRepository<Clazz, Long> {
    Clazz findClazzByName(String clazz);

    List<Clazz> findClazzByQuizzes(List<Quiz> quizzes);
}
