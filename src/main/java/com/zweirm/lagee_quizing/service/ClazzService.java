package com.zweirm.lagee_quizing.service;

import com.zweirm.lagee_quizing.domain.Clazz;
import com.zweirm.lagee_quizing.domain.Quiz;
import com.zweirm.lagee_quizing.repository.ClazzRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClazzService {
    @Autowired
    private ClazzRepository clazzRepository;

    public List<Clazz> findByQuiz(Quiz quiz) {
        List<Quiz> quizList = new ArrayList<>();
        quizList.add(quiz);
        return clazzRepository.findClazzByQuizzes(quizList);
    }
}
