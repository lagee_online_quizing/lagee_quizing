package com.zweirm.lagee_quizing.service;

import com.zweirm.lagee_quizing.domain.Quiz;
import com.zweirm.lagee_quizing.domain.Result;
import com.zweirm.lagee_quizing.domain.StudentUser;
import com.zweirm.lagee_quizing.repository.ResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ResultService {
    @Autowired
    private ResultRepository resultRepository;

    public Result findByQuizAndStudentUser(Quiz quiz, StudentUser studentUser) {
        return resultRepository.findByQuizAndStudentUser(quiz, studentUser);
    }
}
