package com.zweirm.lagee_quizing.service;

import com.zweirm.lagee_quizing.domain.Paper;
import com.zweirm.lagee_quizing.repository.PaperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.List;

@Service
public class PaperService {
    @Autowired
    private PaperRepository paperRepository;

    public List<Paper> showAll(String status) {
        return paperRepository.findAllByStatus(status);
    }

    public void savePaper(Paper paper) {
        paper.setAmount(0);
        paper.setStatus("1");
        paperRepository.save(paper);
    }

    public Paper findPaperById(Long id) {
        return paperRepository.findPaperById(id);
    }

    public void updatePaper(String name, String description, String type, Long id) {
        Paper paper = paperRepository.findPaperById(id);
        paper.setName(name);
        paper.setDescription(description);
        paper.setType(type);
        paperRepository.save(paper);
    }

    public void deletePaper(Long id) {
        Paper paper = paperRepository.findPaperById(id);
        paper.setStatus("0");
        paperRepository.save(paper);
    }
}
