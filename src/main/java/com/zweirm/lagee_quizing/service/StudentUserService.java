package com.zweirm.lagee_quizing.service;

import com.zweirm.lagee_quizing.domain.Clazz;
import com.zweirm.lagee_quizing.domain.StudentUser;
import com.zweirm.lagee_quizing.repository.StudentUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentUserService {
    @Autowired
    private StudentUserRepository studentUserRepository;

    public List<StudentUser> findByClazz(Clazz clazz) {
        return studentUserRepository.findByClazz(clazz);
    }
}
