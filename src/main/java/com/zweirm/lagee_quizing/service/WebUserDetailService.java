package com.zweirm.lagee_quizing.service;

import com.zweirm.lagee_quizing.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * 认证服务
 */
@Component
public class WebUserDetailService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    // 获取数据库数据和前台输入数据进行比对
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return new User(email, userRepository.findUserByEmail(email).getPassword(), AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_TEACHER"));
    }
}
