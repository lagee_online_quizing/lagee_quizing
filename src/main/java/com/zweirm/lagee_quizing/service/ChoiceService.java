package com.zweirm.lagee_quizing.service;

import com.zweirm.lagee_quizing.domain.Choice;
import com.zweirm.lagee_quizing.repository.ChoiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChoiceService {
    @Autowired
    private ChoiceRepository choiceRepository;

    public List<Choice> findByQuestion_Id(Long id) {
        return choiceRepository.findChoiceByQuestion_Id(id);
    }
}
