package com.zweirm.lagee_quizing.service;

import com.zweirm.lagee_quizing.domain.Choice;
import com.zweirm.lagee_quizing.domain.Question;
import com.zweirm.lagee_quizing.repository.ChoiceRepository;
import com.zweirm.lagee_quizing.repository.PaperRepository;
import com.zweirm.lagee_quizing.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class QuestionService {
    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private PaperRepository paperRepository;

    @Autowired
    private ChoiceRepository choiceRepository;

    public List<Question> findAllByPaper_IdAndStatus(Long id, String status) {
        return questionRepository.findAllByPaper_IdAndStatus(id, status);
    }

    public void saveQuestion(
            String title,
            String isRight,
            String answer_1_input,
            String answer_2_input,
            String answer_3_input,
            String answer_4_input,
            String answer_1_illus,
            String answer_2_illus,
            String answer_3_illus,
            String answer_4_illus,
            Long paperId) {
        Question question = new Question();

        List<Choice> choices = new ArrayList<>();

        Choice choice_1 = new Choice();
        choice_1.setContent(answer_1_input);
        choice_1.setIllustration(answer_1_illus);
        choice_1.setQuestion(question);
        choice_1.setRight(false);
        choice_1.setStatus("1");

        Choice choice_2 = new Choice();
        choice_2.setContent(answer_2_input);
        choice_2.setIllustration(answer_2_illus);
        choice_2.setQuestion(question);
        choice_2.setRight(false);
        choice_2.setStatus("1");

        Choice choice_3 = new Choice();
        choice_3.setContent(answer_3_input);
        choice_3.setIllustration(answer_3_illus);
        choice_3.setQuestion(question);
        choice_3.setRight(false);
        choice_3.setStatus("1");

        Choice choice_4 = new Choice();
        choice_4.setContent(answer_4_input);
        choice_4.setIllustration(answer_4_illus);
        choice_4.setQuestion(question);
        choice_4.setRight(false);
        choice_4.setStatus("1");

        switch (isRight) {
            case "1":
                choice_1.setRight(true);
                break;
            case "2":
                choice_2.setRight(true);
                break;
            case "3":
                choice_3.setRight(true);
                break;
            case "4":
                choice_4.setRight(true);
                break;
        }

        choices.add(choice_1);
        choices.add(choice_2);
        choices.add(choice_3);
        choices.add(choice_4);

        question.setStatus("1");
        question.setTitle(title);
        question.setChoices(choices);
        question.setPaper(paperRepository.findPaperById(paperId));

        questionRepository.save(question);
        choiceRepository.save(choice_1);
        choiceRepository.save(choice_2);
        choiceRepository.save(choice_3);
        choiceRepository.save(choice_4);
    }

    public Question findQuestion(Long questionId) {
        return questionRepository.findQuestionById(questionId);
    }

    public List<Choice> findChoices(Long questionId) {
        return choiceRepository.findChoiceByQuestion_Id(questionId);
    }

    public void updateQuestion(
            String title,
            String isRight,
            String answer_1_input,
            String answer_2_input,
            String answer_3_input,
            String answer_4_input,
            String answer_1_illus,
            String answer_2_illus,
            String answer_3_illus,
            String answer_4_illus,
            Long questionId) {
        Question question = questionRepository.findQuestionById(questionId);

        List<Choice> choices = choiceRepository.findChoiceByQuestion_Id(questionId);

        Choice choice_1 = choices.get(0);
        choice_1.setContent(answer_1_input);
        choice_1.setIllustration(answer_1_illus);
        choice_1.setQuestion(question);
        choice_1.setRight(false);
        choice_1.setStatus("1");

        Choice choice_2 = choices.get(1);
        choice_2.setContent(answer_2_input);
        choice_2.setIllustration(answer_2_illus);
        choice_2.setQuestion(question);
        choice_2.setRight(false);
        choice_2.setStatus("1");

        Choice choice_3 = choices.get(2);
        choice_3.setContent(answer_3_input);
        choice_3.setIllustration(answer_3_illus);
        choice_3.setQuestion(question);
        choice_3.setRight(false);
        choice_3.setStatus("1");

        Choice choice_4 = choices.get(3);
        choice_4.setContent(answer_4_input);
        choice_4.setIllustration(answer_4_illus);
        choice_4.setQuestion(question);
        choice_4.setRight(false);
        choice_4.setStatus("1");

        switch (isRight) {
            case "1":
                choice_1.setRight(true);
                break;
            case "2":
                choice_2.setRight(true);
                break;
            case "3":
                choice_3.setRight(true);
                break;
            case "4":
                choice_4.setRight(true);
                break;
        }

        choices.add(choice_1);
        choices.add(choice_2);
        choices.add(choice_3);
        choices.add(choice_4);

        question.setTitle(title);
        question.setChoices(choices);

        questionRepository.save(question);

        choiceRepository.save(choice_1);
        choiceRepository.save(choice_2);
        choiceRepository.save(choice_3);
        choiceRepository.save(choice_4);
    }

    public void deleteQuestion(Long questionId) {
        Question question = questionRepository.findQuestionById(questionId);
        question.setStatus("0");
        questionRepository.save(question);
    }
}
